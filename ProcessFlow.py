from diagrams import Diagram, Cluster, Edge
from diagrams.aws.management import ControlTower
from diagrams.k8s.others import CRD
from diagrams.onprem.inmemory import Redis

with Diagram("ToastPromAlert", show=False):
    with Cluster("Toaster"):
        with Cluster("Schedulers"):
            scheduler = ControlTower("Scheduler")
        with Cluster("Exporters"):
            exporter = CRD("exporter")
        cache = Redis("ExportData")

        server = scheduler >> Edge(color="green", style="dashed") >> exporter >> \
                 Edge(color="green", style="dashed") >> cache
