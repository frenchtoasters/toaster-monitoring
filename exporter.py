from flask import Flask
import time

app = Flask(__name__)


@app.route('/favicon.ico')
def favicon():
    return 200


@app.route('/healthz')
def health():
    return {"data": 'ok'}


@app.route('/export')
def export():
    return {"result": time.time()}
