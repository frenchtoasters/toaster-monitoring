import json
import requests
import time
from apscheduler.schedulers.background import BackgroundScheduler
from bs4 import BeautifulSoup
from pytz import utc


def export_data(endpoint_url: str, data: dict, timeout: int = 30):
    r = requests.get(endpoint_url, timeout=timeout)
    if r.ok:
        soup = BeautifulSoup(r.content, 'html.parser')
        data.update({endpoint_url: json.loads(str(soup))})
    print(data)


cache = {}
endpoints = [
    {'endpoint_url': 'http://localhost:5000/healthz', 'timeout': 10},
    {'endpoint_url': 'http://localhost:5000/export', 'timeout': 10}
]

scheduler = BackgroundScheduler()
scheduler.configure(timezone=utc)
for endpoint in endpoints:
    scheduler.add_job(export_data, 'interval', seconds=10, args=(endpoint['endpoint_url'], cache, endpoint['timeout']))
scheduler.start()

try:
    while True:
        time.sleep(2)
except (KeyboardInterrupt, SystemExit):
    scheduler.shutdown()
