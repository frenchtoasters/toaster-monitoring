import requests
import json
import time
from bs4 import BeautifulSoup
from apscheduler.schedulers.background import BackgroundScheduler
from pytz import utc


class PromCache(object):
    def __init__(self):
        self.cache = {}

    def add(self, key: str, data: json):
        self.cache.update({key: data})

    def dump(self):
        return self.cache

    def __getitem__(self, item):
        return self.cache.get(item)


class PromRules(object):
    def __init__(self, rules: dict = None):
        self.rules = rules

    def __getitem__(self, item):
        return self.rules.get(item)

    def add(self, key: str, rule: dict):
        self.rules.get(key).update(rule)

    def eval(self, key: str, data: dict):
        results = {}
        for rule in self.rules.get(key):
            if rule.get('field') in data.keys():
                if rule.get('state') == data.get(rule.get('field')):
                    results[rule.get('field')] = True
                elif rule.get('state') == 'present' and data.get(rule.get('field')):
                    results[rule.get('field')] = True
                else:
                    results[rule.get('field')] = False
        return results


class PromToast(object):
    def __init__(self, endpoint_configs: list = None, rules: dict = None):
        self.cache = PromCache()
        self.configs = endpoint_configs
        self.rules = PromRules(rules)
        self.results = {}

    def _export_data(self, endpoint_url: str, timeout: int = 30):
        r = requests.get(endpoint_url, timeout=timeout)
        if r.ok:
            soup = BeautifulSoup(r.content, 'html.parser')
            self.cache.add(key=endpoint_url, data=json.loads(str(soup)))

    def execute(self):
        for config in self.configs:
            try:
                self._export_data(endpoint_url=config['endpoint_url'], timeout=config['timeout'])
            except Exception as ExportException:
                raise Exception(ExportException)

    def eval(self):
        for host in self.cache.dump():
            results = self.rules.eval(key=host, data=self.cache.cache.get(host))
            self.results.update(results)
        print(self.results)


if __name__ == '__main__':
    endpoints = [
        {'endpoint_url': 'http://localhost:5000/healthz', 'timeout': 10},
        {'endpoint_url': 'http://localhost:5000/export', 'timeout': 10}
    ]
    monitoring_rules = {
        'http://localhost:5000/healthz': [{'field': 'data', 'state': 'ok'}],
        'http://localhost:5000/export': [{'field': 'result', 'state': 'present'}]
    }
    prom = PromToast(endpoint_configs=endpoints, rules=monitoring_rules)
    scheduler = BackgroundScheduler()
    scheduler.configure(timezone=utc)
    scheduler.add_job(prom.execute, 'interval', seconds=10)
    scheduler.add_job(prom.eval, 'interval', seconds=10)
    scheduler.start()

    try:
        while True:
            time.sleep(2)
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()
